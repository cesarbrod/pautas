---
title: 'Desenvolvendo em Java com Gleice Elen'
author: Regis Tomkiel/regis@doseextra.com
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document
---

**Data:** ?     
**Participantes:** Regis Tomkiel, Perceu Bertoletti    
**Convidado:** Gleice Elen  
**Biografia do convidado:**  
Fundadora do Devs JavaGirl, um grupo de mulheres que desenvolvem, gostam ou se interessam pela linguagem Java.  
Organizadora do Brasília Dev Festival. Um grupo que incentiva a pesquisa, inclusão e expansão do mercado de tecnologia no Distrito Federal.  
***Complementar...***  

Experiência na utilização das seguintes tecnologias:  

* Java  
* Spring MVC  
* JavaScript    
* e mais..  

**Links do convidado:**  

* Twitter: https://twitter.com/gleiceellen  
* Site: ?    
* Blog: ?  
* GitHub: https://github.com/gleiceellen  
* Linkedin: https://www.linkedin.com/in/gleice-ellen-silva-30596b24/  

#### Short Description:
Java é uma linguagem de programação bastante popular e desenvolvida na década de 90  por uma equipe de programadores chefiada por James Gosling, na então Sun Microsystems.  
Diferente de outras linguagem compiladas, onde o resultado é um binário nativo da plataforma, com o Java o resultado é um bytecode interpretado por uma máquina virtual (JVM). Essa característica 
permite enorme portabilidade dos ***binários***, desde computadores x86, celulares, passando por video games, calculadoras e etc.   
Contudo é esta mesma característica que gera uma fama negativa para a tecnologia, com algumas pessoas acusando o Java de ser ***pesado*** e lento.  
Hoje é possível desenvolver em Java para Web, Desktop, Mobile e embarcados, mostrando mais uma vez sua importância.  

#### Episódio:
* **Abertura:**
    + Introdução:
        - Apresentação hosts:
        - Apresentação da convidada (!Ver Links do convidado e Bio)
        - Gancho para convidado: (Por que desenvolver em Java?)
    + Mensagens:  
        - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
        - Links:   
            - Facebook:   
            - Spotify:  
            - Player.fm:  
            - Castbox:  
            - Youtube: (TomkielTV)  
            - Twitch: (Tomkiel)  
            - MEETECH Bento Gonçalves: https://www.facebook.com/groups/MEETECH/     
* **Pauta:**
    + Início do tema: (!Ver Short Description)  
    + O que você vê de positivo no Java?  
    + O que é preciso saber para iniciar com Java?    
    + Qual o ambiente de desenvolvimento ideal para desenvolver em Java?    
    + Java é uma linguagem para quem está iniciando?   
    + Em quais projetos eu devo utilizar Java? (Tipos de projetos)  
    + Onde eu devo procurar material sobre a linguagem? (Recomendações)  
    + Devo me preocupar com performance já no primeiro projeto?  
    + O que você consideraria um erro recorrente com quem começa a estudar e desenvolver com Java?  
    + Como será que está o mercado de trabalho para desenvolvedores em Java?  

* **Finalização:**  
    + Agradecimentos:  
    + Recomendações do convidado:   
        - Uma música:  
        - Um filme ou série:
        - Uma dica livre: (Livro, podcast, site e etc)  


#### Fontes:  
* [https://pt.wikipedia.org/wiki/Java_(linguagem_de_programa%C3%A7%C3%A3o)](https://pt.wikipedia.org/wiki/Java_(linguagem_de_programa%C3%A7%C3%A3o))    
* [https://becode.com.br/java-linguagem-programador-rico/](https://becode.com.br/java-linguagem-programador-rico/)  
* [https://www.meetup.com/pt-BR/brasiliadev/](https://www.meetup.com/pt-BR/brasiliadev/)  
* [https://twitter.com/devsjavagirl](https://twitter.com/devsjavagirl)    
* [https://twitter.com/devsjavagirl](https://twitter.com/devsjavagirl)
