---
title: "Trabalho Home Office (ou Remoto): Experiências individuais"
author: "Regis Tomkiel<regis@doseextra.com>"
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document

---

**Data:** 06/04/2020     
**Participantes:** Regis Tomkiel   
**Convidados:** César Brod, Kleiton Ramil, Kledir Ramil, Gilmara Castro, Mônica Chiesa e Rodrigo Brod

#### **Biografia dos convidados:**  

* ***Kleiton Ramil e Kledir Ramil:***
  + Músicos, escritores, engenheiros formados pela UFRGS com trabalhos icônicos que levam a cultura gaúcha para o Brasil e a cultura brasileira para o mundo.  
    + **Links do convidado:**
      - Twitter: https://twitter.com/  
      - Site: https://    
      - Blog: https://    
      - GitHub: https://github.com/    
      - Linkedin: https://www.linkedin.com/in//  
        &nbsp;
* ***Cesar Brod:***
  + Diretor de relacionamento com comunidades do Linux Professional Institute e dinossauro do HomeOffice.
    + **Links do convidado:**
      - Twitter: https://twitter.com/     
      - Blog: http:// 
      - GitHub: https://github.com/  
      - Gitlab: https://gitlab.com/
      - Linkedin: http://www.linkedin.com/  
* **Gilmara Castro:**
  * Desenvolvedora FullStack e consultora agile na Sysvale SoftGroup.
    * **Links do convidado:**
      * Twitter: 
* **Mônica Chiesa:** 
  * Agente de registro, experimentando pela primeira vez o Home Office.
    * **Links do convidado:**
      * Twitter:
* **Rodrigo Brod:**
  * Coordenador de curso de graduação de design e sócio do estúdio Frente.
    * **Links do convidado:**
      * Twitter:



#### Short Description:

Em mais uma edição do Opencast sobre Home Office trazemos agora mais um povo com diversos níveis de experiência, desde quem já trabalha remotamente a partir do final dos anos 1980 até quem está tendo essa experiência pela primeira vez, gente da área de TI e gente que só tem a TI como ferramenta mesmo. Participarão desse Opencast:.  (!ver Participantes).
...   

#### Short Description 2:

Elaborar intro....

#### Episódio:

* **Abertura:**
  + Introdução:
    - Boas-vindas:
    - Apresentação do convidado (!Ver Links do convidado e Bio)
    - Apresentação hosts:
  + Mensagens:  
    - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
    - Links:      
      - Spotify:
      - iTunes Podcast:
      - Youtube: (TomkielTV)  
      - Twitch: (Tomkiel)     
        &nbsp;  
* **Pauta:**
  + Início do tema: (!Ver Short Description 2)  
  + 
    &nbsp;
* **Finalização:**  
  + Agradecimentos:  
  + Recomendações do convidado:   
    - Uma música:  
    - Um filme ou série:
    - Uma dica livre: (Livro, podcast, site e etc)  

#### Fontes:  

***Opencast podcast 2020***