---
title: "Rotina em Home Office: Como ser produtivo"
author: "Regis Tomkiel<regis@doseextra.com>"
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document
---

**Data:** 01/04/2020     
**Participantes:** Regis Tomkiel, Diego Tumelero e Mateus Roveda   
**Convidados:** Og Maciel e Elyézer Rezende

#### **Biografia dos convidados:**  

* ***Elyézer:***
    + Engenheiro de qualidade na Red Hat, Pythonista, Podcaster, usuário de VIM e mais...
    + Elyézer é o atual mantenedor do [Castálio Podcast]()  
    + Engenheiro de Qualidade Sênior na Red Hat
    + Pythonista
    + Usuário VIM  
        + **Links do convidado:**
            - Twitter: https://twitter.com/elyezer  
            - Site: https://elyezer.com/    
            - Blog: https://elyezer.com/    
            - GitHub: https://github.com/elyezer    
            - Linkedin: https://www.linkedin.com/in/elyezer/  
&nbsp;
* ***Og Maciel:***
    + Diretor de Engenharia de Qualidade na Red Hat
    + Programador Python
    + Escritor
    + Ex podcaster (fundador do Castalio Podcast)
        + **Links do convidado:**
            - Twitter: https://twitter.com/OgMaciel 
            - Amazon: https://amazon.com/author/ogmaciel    
            - Blog: http://omaciel.github.io 
            - GitHub: https://github.com/omaciel  
            - Gitlab: https://gitlab.com/omaciel
            - GoodReads: https://www.goodreads.com/user/show/12048315-og-maciel
            - Linkedin: http://www.linkedin.com/pub/og-maciel/7/425/257/    

#### Short Description:
Desejado por muitos e não compreendido por tantos outros.  
Seria essa uma boa definição para o Home Office?  
No cenário atual, muitas empresas estão descobrindo as vantagens do trabalho remoto e como ele pode manter as engrenagens funcionando.   
Possibilitar que os funcionários realizem suas atividades em outro ambiente traz diversos benefícios não só para os colaboradores, mas também para as empresas em si.   
E talvez, ao contrário do que muitos gestores pensam, ele não reduz a produtividade, nem mesmo a eficiência dessas atividades.  
Os funcionários podem ter uma maior qualidade de vida e passar menos tempo se deslocando para o local de trabalho e as empresas reduzem consideravelmente seus custos, principalmente em infraestrutura.  
E assim que iniciamos nossa conversa sobre a produtividade do trabalho remoto.  

#### Episódio:

* **Abertura:**
    + Introdução:
        - Boas-vindas:
        - Apresentação do convidado (!Ver Links do convidado e Bio)
        - Gancho para convidado: (Og e Elyézer)(O que motivou cada um de vocês à migrar para o Home Office?)
        - Apresentação hosts:
    + Mensagens:  
        - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
        - Links:   
            - Facebook:   
            - Spotify:  
            - Player.fm:  
            - Castbox:  
            - Youtube: (TomkielTV)  
            - Twitch: (Tomkiel)  
            - MEETECH Bento Gonçalves: https://www.facebook.com/groups/MEETECH/   
            &nbsp;  
* **Pauta:**
    + Início do tema: (!Ver Short Description)  
    + 1: O que muda com o Home Office/ trabalho remoto?
    + 2: Por onde começar?
    + 3: Quais as maiores vantagens e dificuldades?
    + 4: Como faço para que minha família/amigos entenda meu formato de trabalho?
    + 5: Trabalho remoto é o futuro ou só uma tendência?
    + 6: O que não pode faltar no Home Office? 
    + 7: Dicas de ferramentas e atividades:
        - Infraestrutura básica
        - Ergonomia
        - Segurança (VPN)  
        &nbsp;
* **Finalização:**  
    + Agradecimentos:  
    + Recomendações do convidado:   
        - Uma música:  
        - Um filme ou série:
        - Uma dica livre: (Livro, podcast, site e etc)  

#### Fontes:  
* [https://adorohomeoffice.com.br/2019/10/07/como-evitar-os-efeitos-psicologicos-negativos-do-home-office/](https://adorohomeoffice.com.br/2019/10/07/como-evitar-os-efeitos-psicologicos-negativos-do-home-office/)   
* [https://forbes.com.br/carreira/2020/03/porque-o-fim-da-pandemia-pode-nao-ser-o-fim-do-home-office/](https://forbes.com.br/carreira/2020/03/porque-o-fim-da-pandemia-pode-nao-ser-o-fim-do-home-office/)  
* [https://pt.wikipedia.org/wiki/Home_office](https://pt.wikipedia.org/wiki/Home_office)  
* [https://exame.abril.com.br/carreira/precisa-fazer-home-office-por-causa-do-coronavirus-confira-as-dicas/](https://exame.abril.com.br/carreira/precisa-fazer-home-office-por-causa-do-coronavirus-confira-as-dicas/)      
* [https://gitlab.com/opencast/pautas/-/blob/master/Rotina_Home_Office_Como_ser_produtivo/compilado_dicas_keyworks.pdf](https://gitlab.com/opencast/pautas/-/blob/master/Rotina_Home_Office_Como_ser_produtivo/compilado_dicas_keyworks.pdf)   



***Opencast podcast 2020***

